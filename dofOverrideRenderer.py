import maya.api.OpenMaya as om
import maya.api.OpenMayaRender as omr
import maya.api.OpenMayaUI as omui

import maya.cmds as cmds


# global instance of this override.
dofRenderer = None


# Using the Maya Python API 2.0.
def maya_useNewAPI():
    pass


def _selectPlug(name):
    sl = om.MSelectionList()
    try:
        sl.add(name)
    except:
        return None

    plg = sl.getPlug(0)

    if plg.isElement and name.find("[") == -1:
        # Hack because MSelectionList returns a plug over the first element of
        # a array of plug when no logical index is given
        return None

    return plg


def nameToNode(name):
    """Returns the MObject matching given name or None if not found.
    Raises RuntimeError if name is ambiguous."""

    plg = _selectPlug(name + ".message")
    return plg.node() if plg else None


def getBoolPlugValue(plug):
    return plug.asBool()


def getFloatPlugValue(plug):
    return plug.asFloat()


def getIntPlugValue(plug):
    return plug.asInt()


class DofOverrideOptions(om.MPxNode):
    kTypeId = om.MTypeId(0x000600E1)
    kTypeName = "DofOverrideOptions"

    # Attributes
    dofEnabled = om.MObject()
    focusDistance = om.MObject()
    # fStop = om.MObject()
    # focusRegionScale = om.MObject()
    camera = om.MObject()
    alpha = om.MObject()
    debugOn = om.MObject()
    maxCocScale = om.MObject()
    searchRadius1 = om.MObject()
    sampleSpacing1 = om.MObject()
    searchRadius2 = om.MObject()
    sampleSpacing2 = om.MObject()

    def __init__(self):
        super(DofOverrideOptions, self).__init__()

    def postConstructor(self):
        om.MNodeMessage.addAttributeChangedCallback(
            self.thisMObject(), DofOverrideOptions.attrChangedCB, self
        )

    @staticmethod
    def creator():
        return DofOverrideOptions()

    @staticmethod
    def initializer():
        numAttrFn = om.MFnNumericAttribute()

        DofOverrideOptions.dofEnabled = numAttrFn.create(
            "dofEnabled", "dfe", om.MFnNumericData.kBoolean
        )
        numAttrFn.default = False
        numAttrFn.storable = True
        numAttrFn.keyable = True
        numAttrFn.readable = True
        numAttrFn.writable = True
        numAttrFn.hidden = True
        om.MPxNode.addAttribute(DofOverrideOptions.dofEnabled)

        DofOverrideOptions.debugOn = numAttrFn.create(
            "debugOn", "don", om.MFnNumericData.kBoolean
        )
        numAttrFn.default = False
        numAttrFn.storable = True
        numAttrFn.keyable = True
        numAttrFn.readable = True
        numAttrFn.writable = True
        numAttrFn.hidden = True
        om.MPxNode.addAttribute(DofOverrideOptions.debugOn)

        DofOverrideOptions.focusDistance = numAttrFn.create(
            "focusDistance", "fde", om.MFnNumericData.kFloat
        )
        numAttrFn.default = 1.0
        numAttrFn.storable = True
        numAttrFn.keyable = True
        numAttrFn.readable = True
        numAttrFn.writable = True
        numAttrFn.hidden = True
        om.MPxNode.addAttribute(DofOverrideOptions.focusDistance)

        DofOverrideOptions.alpha = numAttrFn.create(
            "alpha", "al", om.MFnNumericData.kFloat
        )
        numAttrFn.default = 0.607640
        numAttrFn.storable = True
        numAttrFn.keyable = True
        numAttrFn.readable = True
        numAttrFn.writable = True
        numAttrFn.hidden = False
        om.MPxNode.addAttribute(DofOverrideOptions.alpha)

        DofOverrideOptions.maxCocScale = numAttrFn.create(
            "maxCocScale", "mcs", om.MFnNumericData.kFloat
        )
        numAttrFn.default = 5.0
        numAttrFn.storable = True
        numAttrFn.keyable = True
        numAttrFn.readable = True
        numAttrFn.writable = True
        numAttrFn.hidden = False
        om.MPxNode.addAttribute(DofOverrideOptions.maxCocScale)

        DofOverrideOptions.searchRadius1 = numAttrFn.create(
            "searchRadius1", "sr1", om.MFnNumericData.kInt
        )

        numAttrFn.setMin(1)
        numAttrFn.setMax(20)
        numAttrFn.default = 1
        numAttrFn.storable = True
        numAttrFn.keyable = True
        numAttrFn.readable = True
        numAttrFn.writable = True
        numAttrFn.hidden = False
        om.MPxNode.addAttribute(DofOverrideOptions.searchRadius1)

        DofOverrideOptions.sampleSpacing1 = numAttrFn.create(
            "sampleSpacing1", "sc1", om.MFnNumericData.kInt
        )
        numAttrFn.setMin(1)
        numAttrFn.setMax(20)
        numAttrFn.default = 1
        numAttrFn.storable = True
        numAttrFn.keyable = True
        numAttrFn.readable = True
        numAttrFn.writable = True
        numAttrFn.hidden = False
        om.MPxNode.addAttribute(DofOverrideOptions.sampleSpacing1)

        DofOverrideOptions.searchRadius2 = numAttrFn.create(
            "searchRadius2", "sr2", om.MFnNumericData.kInt
        )
        numAttrFn.setMin(1)
        numAttrFn.setMax(20)
        numAttrFn.default = 1
        numAttrFn.storable = True
        numAttrFn.keyable = True
        numAttrFn.readable = True
        numAttrFn.writable = True
        numAttrFn.hidden = False
        om.MPxNode.addAttribute(DofOverrideOptions.searchRadius2)

        DofOverrideOptions.sampleSpacing2 = numAttrFn.create(
            "sampleSpacing2", "ss2", om.MFnNumericData.kInt
        )
        numAttrFn.setMin(1)
        numAttrFn.setMax(20)
        numAttrFn.default = 1
        numAttrFn.storable = True
        numAttrFn.keyable = True
        numAttrFn.readable = True
        numAttrFn.writable = True
        numAttrFn.hidden = False
        om.MPxNode.addAttribute(DofOverrideOptions.sampleSpacing2)

    @staticmethod
    def attrChangedCB(msg, plg, otherPlug, self):
        cmds.refresh()


class DofSceneRender(omr.MSceneRender):
    def __init__(self, name):
        super(DofSceneRender, self).__init__(name, "Maya_3D_Renderer")

        self.optionsNode = None
        self.panelName = None
        self.params = None

    def preSceneRender(self, context):
        try:
            self.params = self.getParameters()
        except SystemError:
            return

        if self.params is None:
            return

        optionsNodeName = "dofOverrideOptions"

        # Find the options node
        self.optionsNode = nameToNode(optionsNodeName)

        if self.optionsNode is None:
            self.optionsNode = nameToNode(
                cmds.createNode(
                    DofOverrideOptions.kTypeName, name=optionsNodeName, skipSelect=True
                )
            )

        if self.optionsNode is not None:

            optionPlug = om.MPlug(self.optionsNode, DofOverrideOptions.debugOn)
            if optionPlug is not None:
                val = getBoolPlugValue(optionPlug)
                self.params.setParameter("debugOn", val)

            optionPlug = om.MPlug(self.optionsNode, DofOverrideOptions.alpha)
            if optionPlug is not None:
                val = getFloatPlugValue(optionPlug)
                self.params.setParameter("alpha", val)

            optionPlug = om.MPlug(self.optionsNode, DofOverrideOptions.maxCocScale)
            if optionPlug is not None:
                val = getFloatPlugValue(optionPlug)
                self.params.setParameter("maxCocScale", val)

            optionPlug = om.MPlug(self.optionsNode, DofOverrideOptions.searchRadius1)
            if optionPlug is not None:
                val = getIntPlugValue(optionPlug)
                self.params.setParameter("searchRadius1", val)

            optionPlug = om.MPlug(self.optionsNode, DofOverrideOptions.sampleSpacing1)
            if optionPlug is not None:
                val = getIntPlugValue(optionPlug)
                self.params.setParameter("sampleSpacing1", val)

            optionPlug = om.MPlug(self.optionsNode, DofOverrideOptions.searchRadius2)
            if optionPlug is not None:
                val = getIntPlugValue(optionPlug)
                self.params.setParameter("searchRadius2", val)

            optionPlug = om.MPlug(self.optionsNode, DofOverrideOptions.sampleSpacing2)
            if optionPlug is not None:
                val = getIntPlugValue(optionPlug)
                self.params.setParameter("sampleSpacing2", val)

            viewCam = omui.M3dView.active3dView().getCamera()

            dofVal = cmds.getAttr(f"{viewCam}.depthOfField")
            focusDist = cmds.getAttr(f"{viewCam}.focusDistance")

            self.params.setParameter("dofEnabled", dofVal)
            self.params.setParameter("focusDistance", focusDist)


class DofRenderOverride(omr.MRenderOverride):
    def __init__(self, name):
        super(DofRenderOverride, self).__init__(name)

        self.panelName = None

        self.target = omr.MPresentTarget("presentTarget")
        self.dofPass = DofSceneRender("dofSceneRender")
        self.hudRender = omr.MHUDRender()

        self.operations = [self.dofPass, self.hudRender, self.target]

        # set the clear op to use the settings from Maya and disable the
        # clear op embeded in the scene render.
        self.dofPass.clearOperation().setOverridesColors(False)

    def uiName(self):
        return "DOF Override"

    def setup(self, destination):
        super(DofRenderOverride, self).setup(destination)

        self.panelName = destination
        self.operationIndex = -1

    def cleanup(self):
        super(DofRenderOverride, self).cleanup()
        self.operationIndex = -1
        self.panelName = None

    def supportedDrawAPIs(self):
        return omr.MRenderer.kAllDevices

    def startOperationIterator(self):
        self.operationIndex = 0
        return True

    def renderOperation(self):
        return self.operations[self.operationIndex]

    def nextRenderOperation(self):
        self.operationIndex += 1
        return self.operationIndex < len(self.operations)


def initializePlugin(mobject):
    shaderMgr = omr.MRenderer.getShaderManager()
    fragMgr = omr.MRenderer.getFragmentManager()

    shaderMgr.addShaderPath(r".\plug-ins\Shaders\HLSL")
    shaderMgr.addShaderPath(r".\plug-ins\Shaders\Cg")

    fragMgr.addFragmentPath(r".\plug-ins\ScriptFragment")
    fragMgr.addFragmentGraphFromFile("Maya_3D_Renderer.xml")

    plugin = om.MFnPlugin(mobject, "Autodesk", "3.0", "Any")
    plugin.registerNode(
        DofOverrideOptions.kTypeName,
        DofOverrideOptions.kTypeId,
        DofOverrideOptions.creator,
        DofOverrideOptions.initializer,
    )

    global dofRenderer
    dofRenderer = DofRenderOverride("DofRenderOverride")
    omr.MRenderer.registerOverride(dofRenderer)


def uninitializePlugin(mobject):
    global dofRenderer
    plugin = om.MFnPlugin(mobject)
    plugin.deregisterNode(DofOverrideOptions.kTypeId)
    omr.MRenderer.deregisterOverride(dofRenderer)
    dofRenderer = None
